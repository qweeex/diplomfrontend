import axios from "axios";

let api_backend = "http://localhost:8085"

class Api {
    static async GetData(url){
        return axios.get(api_backend + url)
    }

    static async SendData(url, data){
        return axios.post(api_backend + url, data)
    }
}

export default Api
