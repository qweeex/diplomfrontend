import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Reports from "@/views/Reports";
import Orders from "@/views/Orders";
import Points from "@/views/Points";
import Login from "@/views/Login";
import Registation from "@/views/Registation";
import ReportsPoint from "@/views/reports/ReportsPoint";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {auth: true},
    component: Home
  },
  {
    path: '/orders',
    name: 'Orders',
    meta: {auth: true},
    component: Orders
  },
  {
    path: '/reports',
    name: 'Reports',
    meta: {auth: true},
    component: Reports
  },
  {
    path: '/points',
    name: 'Points',
    meta: {auth: true},
    component: Points
  },
  {
    path: '/login',
    name: 'Login',
    meta: {auth: false},
    component: Login
  },
  {
    path: '/reports/points',
    name: 'Points',
    meta: {auth: true},
    component: ReportsPoint
  },
  {
    path: '/register',
    name: 'Register',
    meta: {auth: false},
    component: Registation
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  const data = localStorage.getItem('session')
  const session = JSON.parse(data)
  if (to.matched.some(record => record.meta.auth) && session == null){
    next({
      name: "Login",
      path: "/login"
    });
  } else {
    next()
  }
})

export default router
